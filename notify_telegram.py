from telegram import Bot

from .notifier_bot import get_bot_token


def notify(chat_id, message):
    try:
        bot = Bot(get_bot_token())
        bot.send_message(chat_id=chat_id, text=message)
    except RuntimeError as e:
        print(RuntimeError('Error notifying message').with_traceback(e))


if __name__ == '__main__':
    notify('user_token', 'test')
