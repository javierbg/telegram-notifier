import logging
from pathlib import Path

from telegram.ext import Updater, CommandHandler


base_path = Path(__file__).parent
bot_token_path = base_path / 'TOKEN'


def get_bot_token():
    with bot_token_path.open('r') as f_in:
        token = f_in.read().split('\n')[0]
    return token


def send_user_token(bot, update):
    chat_id = str(update.message.chat_id)
    bot.send_message(chat_id=chat_id,
                     parse_mode='Markdown',
                     text=f"Your token is:\n\n`{chat_id}`\n\nUse this token to get notifications")


if __name__ == '__main__':
    updater = Updater(get_bot_token())
    dispatcher = updater.dispatcher

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO)

    token_handler = CommandHandler('token', send_user_token)
    dispatcher.add_handler(token_handler)

    updater.start_polling()
